﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.DAO;
using WebApplication1.Entidade;
using WebApplication1.Models;
using WebMatrix.WebData;

namespace WebApplication1.Controllers
{
    public class UsuarioController : Controller
    {
        private readonly UsuarioDAO usuarioDAO;
        public UsuarioController(UsuarioDAO UsuarioDAO )
        {
            this.usuarioDAO = UsuarioDAO;
        }

        public ActionResult Index()
        {
            var usuarios = usuarioDAO.GetList();
            return View(usuarios);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Adicionar(UsuarioModel model)
        {
            if(ModelState.IsValid)
            {
                WebSecurity.CreateUserAndAccount(model.Nome, model.Senha, new { Email = model.Email });
                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }

        public ActionResult Form()
        {
            return View();
        }
    }
}