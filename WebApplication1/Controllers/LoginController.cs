﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace WebApplication1.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logar(string usuario, string senha)
        {
            if(WebSecurity.Login(usuario, senha))
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("Login.Erro", "Usuário ou Senha errado");
                return View("Index");
            }
            
        }

        public ActionResult Deslogar()
        {
            WebSecurity.Logout();
            return RedirectToAction("Index", "Home");
        }
    }
}