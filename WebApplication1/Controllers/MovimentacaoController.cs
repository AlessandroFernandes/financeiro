﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.DAO;
using WebApplication1.Entidade;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class MovimentacaoController : Controller
    {
        private readonly UsuarioDAO usuarioDAO;
        private readonly MovimentacaoDAO movimentacaoDAO;
        public MovimentacaoController(UsuarioDAO UsuarioDAO, MovimentacaoDAO MovimentacaoDAO)
        {
            this.usuarioDAO = UsuarioDAO;
            this.movimentacaoDAO = MovimentacaoDAO;
        }
    
        public ActionResult Index()
        {
            return View(movimentacaoDAO.GetList());
        }

        public ActionResult Form()
        {
            ViewBag.Usuarios = usuarioDAO.GetList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Adicionar(Movimentacao movimentacao)
        {
            if(ModelState.IsValid)
            {
                movimentacaoDAO.Adicionar(movimentacao);
                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", movimentacao);
            }
        }

    }
}