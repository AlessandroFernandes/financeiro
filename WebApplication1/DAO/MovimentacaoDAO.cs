﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Entidade;

namespace WebApplication1.DAO
{
    public class MovimentacaoDAO
    {
        private readonly FinanceiroDAO financeiroDAO;
        public MovimentacaoDAO(FinanceiroDAO contexto)
        {
            this.financeiroDAO = contexto;
        }

        public void Adicionar(Movimentacao movimentacao)
        {
            financeiroDAO.Movimentacao.Add(movimentacao);
            financeiroDAO.SaveChanges();
        }

        public IList<Movimentacao> GetList()
        {
            return financeiroDAO.Movimentacao.ToList();
        }
    }
}