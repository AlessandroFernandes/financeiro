﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication1.Entidade;

namespace WebApplication1.DAO
{
    public class FinanceiroDAO : DbContext
    {
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Movimentacao> Movimentacao { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movimentacao>().HasRequired(x => x.Usuario);
        }
    }
}