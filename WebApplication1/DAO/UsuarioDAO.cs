﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Entidade;

namespace WebApplication1.DAO
{
    public class UsuarioDAO
    {
        private readonly FinanceiroDAO context;
        public UsuarioDAO(FinanceiroDAO context)
        {
            this.context = context;
        }

        public void Adicionar(Usuario Usuario)
        {
            context.Usuario.Add(Usuario);
            context.SaveChanges();
        }

        public IList<Usuario> GetList()
        {
            return context.Usuario.ToList();
        }
    }
}