namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TabelaCorrigida : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Movimentacaos", name: "Usuario_Id", newName: "UsuarioId");
            RenameIndex(table: "dbo.Movimentacaos", name: "IX_Usuario_Id", newName: "IX_UsuarioId");
            DropColumn("dbo.Movimentacaos", "UsusarioId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Movimentacaos", "UsusarioId", c => c.Int(nullable: false));
            RenameIndex(table: "dbo.Movimentacaos", name: "IX_UsuarioId", newName: "IX_Usuario_Id");
            RenameColumn(table: "dbo.Movimentacaos", name: "UsuarioId", newName: "Usuario_Id");
        }
    }
}
